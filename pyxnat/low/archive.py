from pyxnat.rest import archive
from pyxnat.low.helpers import get_entity, create_entity, delete_entity
from pyxnat.low.helpers import download_file, upload_file
from pyxnat.low.helpers import make_query_params
from pyxnat.low.helpers import norm_file_id

# ---------------------------------------------------------------------------
# Projects
# ---------------------------------------------------------------------------


# TODO: parameter ?activeSince=DATE
def get_projects(server, accessible=True,
                 owner=True, member=True,
                 active_since='', recent=True,
                 favorite=True):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226066
    {'insert_user': 'nosetests',
    'description': '',
    'user_role_204': 'Owners',
    'project_invs': '',
    'quarantine_status': 'active',
    'last_accessed_204': '2013-02-28 09:46:50.369274',
    'secondary_id': 'nosetests',
    'insert_date': '2009-12-01 04:27:58.0',
    'pi': '',
    'id': 'nosetests',
    'project_access_img': '/@WEBAPPimages/key.gif',
    'name': 'nosetests'}
    """
    url = server.resource('archive', 'projects')
    status, resp, data = url.get_json(format='csv', accessible=accessible,
                                      owner=owner, member=member,
                                      recent=recent, favorite=favorite)
    return data


def get_project(server, project_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226079
    """
    url = server.resource('archive', 'projects', project_id)
    return get_entity(url)


def create_project(server, project_id, safe=True):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226079
    """
    url = server.resource('archive', 'projects', project_id)
    return create_entity(url, safe)


def delete_project(server, project_id, safe=True):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226079
    """
    url = server.resource('archive', 'projects', project_id)
    return delete_entity(url, safe)


def get_project_accessibility(server, project_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226064
    """
    url = server.resource('archive', 'projects', project_id, 'accessibility')
    status, resp, data = url.get()
    return data.read()


# XNAT: should set mode on the same URL with a POST ?mode=blah
def set_project_accessibility(server, project_id, mode):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226065

        Parameters
        ----------
        mode: str
            public, protected, or private
    """
    url = server.resource('archive', 'projects', project_id,
                          'accessibility', mode)
    status, resp, data = url.put()
    # implement fix to raise Exception
    return True


def get_project_archive_folder(server, project_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226062
    """
    url = server.resource('archive', 'projects', project_id,
                          'current_arc')
    status, resp, data = url.get()
    return data.read()


def set_project_archive_folder(server, project_id, folder):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226063
    """
    url = server.resource('archive', 'projects', project_id,
                          'current_arc', folder)
    status, resp, data = url.put()
    return True


def get_project_prearchive_code(server, project_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226067

        Returns
        -------
        0: all data placed in prearchive (default);
        4: all sessions will be auto-archived if they match a
           pre-existing project and they don't duplicate files
           already archived.
        5: All image data will be placed into the archive automatically
           and will overwrite existing files. Data which doesn't match
           a pre-existing project will be placed in an 'Unassigned' project.
    """
    url = server.resource('archive', 'projects', project_id, 'prearchive_code')
    status, resp, data = url.get()
    return int(data.read())


def set_project_prearchive_code(server, project_id, code):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226125

        Parameters
        ----------
        code: int
            0: all data placed in prearchive (default);
            4: all sessions will be auto-archived if they match a
               pre-existing project and they don't duplicate files
               already archived.
            5: All image data will be placed into the archive automatically
               and will overwrite existing files. Data which doesn't match a
               pre-existing project will be placed in an 'Unassigned' project.
    """
    url = server.resource('archive', 'projects', project_id,
                          'prearchive_code', unicode(code))
    status, resp, data = url.put()
    return True


def get_project_quarantine_code(server, project_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226068

        integer (0-1)
    """
    url = server.resource('archive', 'projects', project_id, 'quarantine_code')
    status, resp, data = url.get()
    return int(data.read())


def set_project_quarantine_code(server, project_id, code):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226069

        integer (0-1)
    """
    url = server.resource('archive', 'projects', project_id,
                          'quarantine_code', unicode(code))
    status, resp, data = url.put()
    return True


def get_project_users(server, project_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226211
    """
    url = server.resource('archive', 'projects', project_id, 'users')
    status, resp, data = url.get_json(format='csv')
    return data


def add_project_user(server, project_id, user_id, user_role):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226209
        https://wiki.xnat.org/pages/viewpage.action?pageId=6226210
        https://wiki.xnat.org/pages/viewpage.action?pageId=6226208
    """
    url = server.resource('archive', 'projects', project_id,
                          'users', user_role.title(), user_id)
    status, resp, data = url.put(format='json')
    return data


def remove_project_user(server, project_id, user_id, user_role):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226209
        https://wiki.xnat.org/pages/viewpage.action?pageId=6226210
        https://wiki.xnat.org/pages/viewpage.action?pageId=6226208

        Parameters
        ----------
        user_role: str
            collaborators, members or owners
    """
    url = server.resource('archive', 'projects', project_id,
                          'users', user_role.title(), user_id)
    status, resp, data = url.delete(format='json')
    return data


# ---------------------------------------------------------------------------
# Subjects
# ---------------------------------------------------------------------------

def get_subjects(server, project_id=None, fields=None, filters=None):
    """ !! URL not documented
    {'insert_user': 'nosetests',
    'insert_date': '2011-05-31 03:06:01.0',
    'URI': '/data/subjects/unique_s001',
    'label': 'unique_s001',
    'project': 'PB',
    'ID': 'unique_s001'}
    """
    # XNAT: url not documented
    url = server.resource('archive', 'subjects')
    query_params = make_query_params(project_id=project_id,
                                     fields=fields,
                                     filters=filters)
    status, resp, data = url.get_json(format='csv', **query_params)
    return data


def get_subject(server, project_id, subject_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226078
    """
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id)
    return get_entity(url)


def create_subject(server, project_id, subject_id,
                   subject_label=None, safe=True):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226078
    """
    if subject_label is not None:
        url = server.resource('archive', 'projects', project_id,
                              'subjects', subject_label)
        create_entity(url, safe, **dict(ID=subject_id))
    else:
        url = server.resource('archive', 'projects', project_id,
                              'subjects', subject_id)
        create_entity(url, safe)


def delete_subject(server, project_id, subject_id, safe=True):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226078
    """
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id)
    delete_entity(url, safe)


def get_subject_resources(server, project_id, subject_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id, 'resources')
    status, resp, data = url.get_json(format='csv')
    return data


def get_subject_resource(server, project_id, subject_id, resource_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'resources', resource_id)
    return get_entity(url)


def create_subject_resource(server, project_id, subject_id,
                            resource_id, safe=True):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'resources', resource_id)
    return create_entity(url, safe)


def get_subject_resource_files(server, project_id, subject_id, resource_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'resources', resource_id, 'files')
    status, resp, data = url.get_json(format='csv')
    return data


def get_subject_resource_file(server, project_id,
                              subject_id, resource_id, file_id):
    url = server.resource(
        'archive', 'projects', project_id,
        'subjects', subject_id,
        'resources', resource_id,
        'files', *norm_file_id(file_id))

    return download_file(url)


def create_subject_resource_file(server, project_id,
                                 subject_id, resource_id, file_id,
                                 data, safe=True):
    url = server.resource(
        'archive', 'projects', project_id,
        'subjects', subject_id,
        'resources', resource_id,
        'files', *norm_file_id(file_id))

    return upload_file(url, data=data, safe=safe)


# ---------------------------------------------------------------------------
# Experiments
# ---------------------------------------------------------------------------

def get_experiments(server, project_id=None, subject_id=None,
                    data_type=None, fields=None, filters=None):
    """
    {'insert_date': '2009-06-02 06:12:50.0',
     'xsiType': 'xnat:mrSessionData',
     'URI': '/data/experiments/CENTRAL_E00457',
     'label': 'OAS2_0185_MR2',
     'project': 'CENTRAL_OASIS_LONG',
     'date': '',
     'ID': 'CENTRAL_E00457'}
     """
    url = server.resource('archive', 'experiments')

    query_params = make_query_params(project_id=project_id,
                                     subject_id=subject_id,
                                     data_type=data_type,
                                     fields=fields,
                                     filters=filters)

    status, resp, data = url.get_json(format='csv', **query_params)
    return data


def get_experiment(server, project_id, subject_id, experiment_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id)
    return get_entity(url)


def create_experiment(server, project_id, subject_id, experiment_id,
                      experiment_label=None,
                      data_type='xnat:mrSessionData', safe=True):

    query_params = make_query_params(data_type=data_type)
    # XNAT: error with experiment custom id
    if experiment_label is not None:
        url = server.resource('archive', 'projects', project_id,
                              'subjects', subject_id,
                              'experiments', experiment_label)
        return create_entity(url, safe,
                             ID=experiment_id, **query_params)
    else:
        url = server.resource('archive', 'projects', project_id,
                              'subjects', subject_id,
                              'experiments', experiment_id)
        return create_entity(url, safe, **query_params)


def delete_experiment(server, project_id, subject_id,
                      experiment_id, safe=True):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id)
    return delete_entity(url, safe)


def get_experiment_resources(server, project_id, subject_id, experiment_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id, 'resources')
    status, resp, data = url.get_json(format='csv')
    return data


def get_experiment_resource(server, project_id, subject_id,
                            experiment_id, resource_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'resources', resource_id)
    return get_entity(url)


def create_experiment_resource(server, project_id, subject_id,
                            experiment_id, resource_id, safe=True):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'resources', resource_id)
    return create_entity(url, safe)


def get_experiment_resource_files(server, project_id, subject_id,
                                  experiment_id, resource_id):

    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'resources', resource_id, 'files')
    status, resp, data = url.get_json(format='csv')
    return data


def get_experiment_resource_file(server, project_id, subject_id,
                                 experiment_id, resource_id, file_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'resources', resource_id,
                          'files', *norm_file_id(file_id))
    return download_file(url)


def create_experiment_resource_file(server, project_id, subject_id,
                                    experiment_id, resource_id, file_id,
                                    data, safe=True):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'resources', resource_id,
                          'files', *norm_file_id(file_id))
    return upload_file(url, data=data, safe=safe)


# ---------------------------------------------------------------------------
# Assessors
# ---------------------------------------------------------------------------

def get_assessors(server,  project_id, subject_id,
                  experiment_id, fields=None, filters=None):
    """
    {'ID': 'OAS1_0453_MR1_APARC_LH',
    'URI': '/data/experiments/OAS1_0453_MR1_APARC_LH',
    'date': '',
    'insert_date': '2007-10-04 12:23:32.0',
    'label': 'OAS1_0453_MR1_APARC_LH',
    'project': 'CENTRAL_OASIS_CS',
    'session_ID': 'OAS1_0453_MR1',
    'session_label': 'OAS1_0453_MR1',
    'xnat:imageassessordata/id': 'OAS1_0453_MR1_APARC_LH',
    'xsiType': 'fs:aparcRegionAnalysis'}
    """

    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id, 'assessors')
    query_params = make_query_params(fields=fields, filters=filters)
    status, resp, data = url.get_json(format='csv', **query_params)
    return data


def get_assessor(server, project_id, subject_id, experiment_id, assessor_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'assessors', assessor_id)
    return get_entity(url)


def create_assessor(server, project_id, subject_id, experiment_id,
                    assessor_id, assessor_label=None,
                    data_type='xnat:mrAssessorData', safe=True):
    query_params = make_query_params(data_type=data_type)
    # XNAT: error with assessor custom id
    if assessor_label is not None:
        url = server.resource('archive', 'projects', project_id,
                              'subjects', subject_id,
                              'experiments', experiment_id,
                              'assessors', assessor_label)
        return create_entity(url, safe, ID=assessor_id, **query_params)
    else:
        url = server.resource('archive', 'projects', project_id,
                              'subjects', subject_id,
                              'experiments', experiment_id,
                              'assessors', assessor_id)
        return create_entity(url, safe, **query_params)


# XNAT: assessors don't seem to be deletable
def delete_assessor(server, project_id, subject_id,
                    experiment_id, assessor_id, safe=True):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'assessors', assessor_id)
    return delete_entity(url, safe)


def get_assessor_resources(server, project_id, subject_id,
                           experiment_id, assessor_id, inout):
    """
    {'category': 'assessors',
    'cat_id': 'OAS1_0457_MR1_APARC_LH',
    'label': '',
    'xnat_abstractresource_id': '6830',
    'cat_desc': 'Freesurfer APARC',
    'element_name': 'xnat:resource'}
    """

    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'assessors', assessor_id, inout,
                          'resources')
    status, resp, data = url.get_json(format='csv')
    return data


def get_assessor_resource(server, project_id, subject_id, experiment_id,
                          assessor_id, inout, resource_id, safe=True):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'assessors', assessor_id, inout,
                          'resources', resource_id)
    return create_entity(url, safe)


def create_assessor_resource(server, project_id, subject_id,
                             experiment_id, assessor_id, inout, resource_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'assessors', assessor_id, inout,
                          'resources', resource_id)
    return get_entity(url)


def get_assessor_resource_files(server, project_id, subject_id,
                                experiment_id, assessor_id, inout,
                                resource_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'assessors', assessor_id, inout,
                          'resources', resource_id, 'files')
    status, resp, data = url.get_json(format='csv')
    return data


def get_assessor_resource_file(server, project_id, subject_id,
                               experiment_id, assessor_id, inout,
                               resource_id, file_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'assessors', assessor_id, inout,
                          'resources', resource_id,
                          'files', *norm_file_id(file_id))
    return download_file(url)


def create_assessor_resource_file(server, project_id, subject_id,
                                  experiment_id, assessor_id, inout,
                                  resource_id, file_id, data, safe=True):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'assessors', assessor_id, inout,
                          'resources', resource_id,
                          'files', *norm_file_id(file_id))
    return upload_file(url, data=data, safe=safe)


# ---------------------------------------------------------------------------
# Reconstructions
# ---------------------------------------------------------------------------

def get_reconstructions(server,  project_id, subject_id,
                  experiment_id, fields=None, filters=None):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'reconstructions')
    query_params = make_query_params(fields=fields, filters=filters)
    status, resp, data = url.get_json(format='csv', **query_params)
    return data


def get_reconstruction(server, project_id, subject_id,
                       experiment_id, reconstruction_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'reconstructions', reconstruction_id)
    return get_entity(url)


def create_reconstruction(server, project_id, subject_id, experiment_id,
                      reconstruction_id, reconstruction_label=None, safe=True):
    if reconstruction_label is not None:
        url = server.resource('archive', 'projects', project_id,
                              'subjects', subject_id,
                              'experiments', experiment_id,
                              'reconstructions', reconstruction_label)
        return create_entity(url, safe, **dict(ID=reconstruction_id))
    else:
        url = server.resource('archive', 'projects', project_id,
                              'subjects', subject_id,
                              'experiments', experiment_id,
                              'reconstructions', reconstruction_id)
        return create_entity(url, safe)


def delete_reconstruction(server, project_id, subject_id,
                    experiment_id, reconstruction_id, safe=True):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'reconstructions', reconstruction_id)
    return delete_entity(url, safe)


def get_reconstruction_resources(server, project_id, subject_id,
                           experiment_id, reconstruction_id, inout):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'reconstructions', reconstruction_id, inout,
                          'resources')
    status, resp, data = url.get_json(format='csv')
    return data


def get_reconstruction_resource(server, project_id, subject_id, experiment_id,
                                reconstruction_id, inout, resource_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'reconstructions', reconstruction_id, inout,
                          'resources', resource_id)
    return get_entity(url)


def create_reconstruction_resource(server, project_id, subject_id,
                                   experiment_id, reconstruction_id,
                                   inout, resource_id, safe=True):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'reconstructions', reconstruction_id, inout,
                          'resources', resource_id)
    return create_entity(url, safe)


def get_reconstruction_resource_files(server, project_id, subject_id,
                                      experiment_id, reconstruction_id, inout,
                                      resource_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'reconstructions', reconstruction_id, inout,
                          'resources', resource_id,
                          'files')
    status, resp, data = url.get_json(format='csv')
    return data


def get_reconstruction_resource_file(server, project_id, subject_id,
                               experiment_id, reconstruction_id, inout,
                               resource_id, file_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'reconstructions', reconstruction_id, inout,
                          'resources', resource_id,
                          'files', *norm_file_id(file_id))
    return download_file(url)


def create_reconstruction_resource_file(server, project_id, subject_id,
                               experiment_id, reconstruction_id, inout,
                               resource_id, file_id, data, safe=True):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'reconstructions', reconstruction_id, inout,
                          'resources', resource_id,
                          'files', *norm_file_id(file_id))
    return upload_file(url, data=data, safe=safe)


# ---------------------------------------------------------------------------
# Scans
# ---------------------------------------------------------------------------

def get_scans(server,  project_id, subject_id,
                  experiment_id, fields=None, filters=None):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226186

    {'xsiType': 'xnat:mrScanData',
    'type': 'MPRAGE',
    'series_description': '',
    'xnat_imagescandata_id': '467',
    'URI': '/data/experiments/OAS1_0457_MR1/scans/mpr-1',
    'note': '',
    'quality': 'usable',
    'ID': 'mpr-1'}
    """
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'scans')
    query_params = make_query_params(fields=fields, filters=filters)
    status, resp, data = url.get_json(format='csv', **query_params)
    return data


def get_scan(server, project_id, subject_id, experiment_id, scan_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226187
    """
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'scans', scan_id)
    return get_entity(url)


def create_scan(server, project_id, subject_id, experiment_id,
                      scan_id, data_type='xnat:mrScanData', safe=True):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226187
    """
    query_params = make_query_params(data_type=data_type)
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'scans', scan_id)
    return create_entity(url, safe, **query_params)


def delete_scan(server, project_id, subject_id,
                experiment_id, scan_id, safe=True):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226187
    """
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'scans', scan_id)
    delete_entity(url, safe)


def get_scan_resources(server, project_id, subject_id,
                       experiment_id, scan_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'scans', scan_id, 'resources')
    status, resp, data = url.get_json(format='csv')
    return data


def get_scan_resource(server, project_id, subject_id,
                      experiment_id, scan_id, resource_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'scans', scan_id,
                          'resources', resource_id)
    return get_entity(url)


def create_scan_resource(server, project_id, subject_id,
                      experiment_id, scan_id, resource_id, safe=True):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'scans', scan_id,
                          'resources', resource_id)
    return create_entity(url, safe)


def get_scan_resource_files(server, project_id, subject_id,
                            experiment_id, scan_id, resource_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'scans', scan_id,
                          'resources', resource_id,
                          'files')
    status, resp, data = url.get_json(format='csv')
    return data


def get_scan_resource_file(server, project_id, subject_id,
                           experiment_id, scan_id,
                           resource_id, file_id):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'scans', scan_id,
                          'resources', resource_id,
                          'files', *norm_file_id(file_id))
    return download_file(url)


def create_scan_resource_file(server, project_id, subject_id,
                              experiment_id, scan_id,
                              resource_id, file_id, data, safe=True):
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'scans', scan_id,
                          'resources', resource_id,
                          'files', *norm_file_id(file_id))
    return upload_file(url, data=data, safe=safe)


# ---------------------------------------------------------------------------
# Project Sharing Resources
# ---------------------------------------------------------------------------

def get_assessor_shared_projects(server, project_id, subject_id,
                                 experiment_id, assessor_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226165
    """
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'assessors', assessor_id, 'projects')
    status, resp, data = url.get_json(format='csv')
    return data


def set_assessor_share_project(server, project_id, subject_id,
                               experiment_id, assessor_id, share_project_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226163
    """
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'assessors', assessor_id,
                          'projects', share_project_id)
    status, resp, data = url.put()
    return True


def get_experiment_shared_projects(server, project_id, subject_id,
                                   experiment_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226161
    """
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'projects')
    status, resp, data = url.get_json(format='csv')
    return data


def set_experiment_share_project(server, project_id, subject_id,
                                 experiment_id, share_project_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226159
    """
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'experiments', experiment_id,
                          'projects', share_project_id)
    status, resp, data = url.put()
    return True


def get_subject_shared_projects(server, project_id, subject_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226157
    """
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id, 'projects')
    status, resp, data = url.get_json(format='csv')
    return data


def set_subject_share_project(server, project_id, subject_id,
                              share_project_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226155
    """
    url = server.resource('archive', 'projects', project_id,
                          'subjects', subject_id,
                          'projects', share_project_id)
    status, resp, data = url.put()
    return True
