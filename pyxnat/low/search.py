from .helpers import build_xnat_search
from .helpers import build_search_document


"""
Search URLs:
POST             /search
GET              /search/saved
GET, PUT, DELETE /search/saved/{search_id}
GET, PUT, DELETE /search/saved/{search_id}/results
GET              /search/elements
GET              /search/element/{data_type}
"""


def get_saved(server):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226140

    {'allow_diff_columns': '0',
    'description': 'full description',
    'extension': '246',
    'sort_by_field_id': '',
    'stored_search_info': '756',
    'sort_by_element_name': '',
    'brief_description': 'test new api',
    'tag': '',
    'layeredsequence': '',
    'root_element_name': 'xnat:mrSessionData',
    'id': 'test%20new%20api',
    'secure': '0'}
    """
    url = server.resource('search', 'saved')
    status, resp, data = url.get_json(format='csv')
    return data


def get_search_results(server, brief_description):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226144

    {'quarantine_status': 'active',
    'xnat_subjectdata_subject_id': 'CENTRAL_S00026',
    'projects': '<Calib>',
    'session_id': 'CENTRAL_E00017'}
    """
    url = server.resource('search', 'saved',
                          brief_description, 'results')
    status, resp, data = url.get_json(format='csv')
    return data


def get_search_document(server, search_id):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226142
    """
    url = server.resource('search', 'saved', search_id)
    status, resp, data = url.get(format='xml')
    return data.read()


def get_data_types(server):
    """ https://wiki.xnat.org/display/XNAT16/Query+the+XNAT+Search+Engine+with+REST+API

    {'COUNT': '465',
    'ELEMENT_NAME': 'cnda:modifiedScheltensData',
    'PLURAL': 'Mod Scheltens',
    'SECURED': 'true',
    'SINGULAR': 'Mod Scheltens'}
    """
    url = server.resource('search', 'elements')
    status, resp, data = url.get_json(format='csv')
    return data


def get_data_fields(server, data_type):
    """ https://wiki.xnat.org/display/XNAT16/Query+the+XNAT+Search+Engine+with+REST+API

    {'DESC': 'Label within the surfmask_smpl project.',
    'ELEMENT_NAME': 'cnda:modifiedScheltensData',
    'FIELD_ID': 'CNDA_MODIFIEDSCHELTENSDATA_PROJECT_IDENTIFIER=surfmask_smpl',
    'HEADER': 'surfmask_smpl',
    'REQUIRES_VALUE': 'false',
    'SRC': '2',
    'SUMMARY': 'Label within the surfmask_smpl project.',
    'TYPE': 'string'}
    """
    url = server.resource('search', 'elements', data_type)
    status, resp, data = url.get_json(format='csv')

    fields = []
    for row in data:
        field = row.get('FIELD_ID')
        if '=' not in field and 'SHARINGSHAREPROJECT' not in field:
            fields.append(row)
    return fields


def get_field_values(server, field):
    data_type, xnat_field = field.split('/')
    data = post(server, data_type, [field], [(field, 'LIKE', '%'), 'AND'])
    return sorted(set([row.get(xnat_field.lower()) for row in data]))


def save(server, brief_description, data_type, columns, filters,
         description=None, share='private', safe=True):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226142
    """
    if share == 'private':
        users = [server.resource.credentials[0]]
    elif share == 'public':
        users = []
    elif isinstance(share, list):
        users = share
    if description is None:
        description = ''

    bundle = build_xnat_search(data_type, columns,
                               filters, brief_description,
                               description.replace('%', '%%'),
                               users)
    url = server.resource('search', 'saved', brief_description)
    status, resp, data = url.put_json(body=bundle, format='csv')
    # XNAT: return 201 (created) on success, 400 (bad request) on failure
    # -- FIX -- #
    status = 201 if is_valid(server, brief_description) else 400

    if status == 400 and safe:
        delete(server, brief_description)  # FIX
        raise Exception('%s Bad Request: query "%s" not saved' % (
                status, brief_description))

    return status


def delete(server, brief_description, safe=True):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226142
    """
    url = server.resource('search', 'saved', brief_description)
    status, resp, data = url.delete()
    # XNAT: return 204 (no content) on success, 404 (not found) on failure
    # -- FIX -- #
    status = 204 if not is_valid(server, brief_description) else 404

    if status == 404 and safe:
        raise Exception('%s Not Found: cannot find query "%s"'
                        % (status, brief_description))

    return status


def post(server, data_type, columns, filters):
    """ https://wiki.xnat.org/pages/viewpage.action?pageId=6226169
    {'quarantine_status': 'active',
    'xnat_subjectdata_subject_id': 'CENTRAL_S00026',
    'projects': '<Calib>',
    'session_id': 'CENTRAL_E00017'}
    """
    bundle = build_xnat_search(data_type, columns, filters)
    url = server.resource('search')
    status, resp, data = url.post_json(body=bundle, format='csv')
    # do something with status?
    return data


def is_valid(server, brief_description):
    try:
        get_search_results(server, brief_description)
        return True
    except:
        return False
