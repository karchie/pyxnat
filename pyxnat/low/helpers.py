import exceptions
import xml.etree.ElementTree as etree


def is_valid(url):
    try:
        status, resp, data = url.head()
        return True
    except:
        return False


def get_entity(url):
    status, resp, data = url.get(format='xml')
    return data.read()


def download_file(url):
    status, resp, data = url.get()
    return data


def upload_file(url, data, safe=True, *args, **kwargs):
    try:
        status, resp, data = url.put(body=data, inbody=True, *args, **kwargs)
    except:
        status = 400
    status = 201 if status in [200, 201] else 400
    if status == 400 and safe:
        raise Exception('%s Bad Request: resource not created' % status)
    return status


# needs something to say resource already exists
def create_entity(url, safe, *args, **kwargs):
    status, resp, data = url.put(*args, **kwargs)
    status = 201 if status in [200, 201] else 400
    if status == 400 and safe:
        raise Exception('%s Bad Request: resource not created' % status)
    return status


def delete_entity(url, safe, *args, **kwargs):
    status, resp, data = url.delete(*args, **kwargs)
    status = 204 if status == 200 else 404
    if status == 404 and safe:
        raise Exception('%s Not Found: resource not deleted' % status)
    return status


def make_query_params(project_id=None, subject_id=None,
                      data_type=None, fields=None, filters=None):
    query_params = {}
    if filters is None:
        filters = {}

    def set_param(name, param):
        if param is not None and param not in ['', []]:
            if isinstance(param, list):
                param = ','.join(param)
            query_params[name] = param

    set_param('project', project_id)
    set_param('subject_ID', subject_id)
    set_param('xsiType', data_type)
    set_param('columns', fields)

    for k, v in filters.items():
        set_param(k, v)

    return query_params


def norm_file_id(file_id):
    if isinstance(file_id, (str, unicode)):
        return [file_id]
    else:
        return file_id


search_nsmap = {'xdat': 'http://nrg.wustl.edu/security',
                'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}

special_ops = {'*': '%', }


def build_search_document(root_element_name, columns, criteria_set,
                          brief_description='', long_description='',
                          allowed_users=[]):

    root_node = etree.Element(
        etree.QName(search_nsmap['xdat'], 'bundle'),
        nsmap=search_nsmap)

    root_node.set('ID', "@%s" % root_element_name)
    root_node.set('brief-description', brief_description)
    root_node.set('description', long_description)
    root_node.set('allow-diff-columns', "0")
    root_node.set('secure', "false")

    root_element_name_node = etree.Element(
        etree.QName(search_nsmap['xdat'], 'root_element_name'),
        nsmap=search_nsmap)

    root_element_name_node.text = root_element_name
    root_node.append(root_element_name_node)

    for i, column in enumerate(columns):
        element_name, field_ID = column.split('/')

        search_field_node = etree.Element(
            etree.QName(search_nsmap['xdat'], 'search_field'),
            nsmap=search_nsmap)

        element_name_node = etree.Element(
            etree.QName(search_nsmap['xdat'], 'element_name'),
            nsmap=search_nsmap)

        element_name_node.text = element_name

        field_ID_node = etree.Element(
            etree.QName(search_nsmap['xdat'], 'field_ID'),
            nsmap=search_nsmap)

        field_ID_node.text = field_ID

        sequence_node = etree.Element(
            etree.QName(search_nsmap['xdat'], 'sequence'),
            nsmap=search_nsmap)

        sequence_node.text = str(i)

        type_node = etree.Element(
            etree.QName(search_nsmap['xdat'], 'type'),
            nsmap=search_nsmap)

        type_node.text = 'string'

        header_node = etree.Element(
            etree.QName(search_nsmap['xdat'], 'header'),
            nsmap=search_nsmap)

        header_node.text = column

        search_field_node.extend([
                element_name_node,
                field_ID_node,
                sequence_node,
                type_node, header_node
                ])

        root_node.append(search_field_node)

    search_where_node = etree.Element(
        etree.QName(search_nsmap['xdat'], 'search_where'),
        nsmap=search_nsmap)

    root_node.append(build_criteria_set(search_where_node, criteria_set))

    if allowed_users != []:

        allowed_users_node = etree.Element(
            etree.QName(search_nsmap['xdat'], 'allowed_user'),
            nsmap=search_nsmap)

        for allowed_user in allowed_users:
            login_node = etree.Element(
                etree.QName(search_nsmap['xdat'], 'login'),
                nsmap=search_nsmap)

            login_node.text = allowed_user
            allowed_users_node.append(login_node)

        root_node.append(allowed_users_node)

    # xml.etree does not handle nsmap attribute for serialization
    for node in root_node.getiterator():
        if node.get('nsmap'):
            del node.attrib['nsmap']

    return etree.tostring(root_node)


def build_criteria_set(container_node, criteria_set):

    for criteria in criteria_set:
        if isinstance(criteria, basestring):
            container_node.set('method', criteria)

        if isinstance(criteria, (list)):
            sub_container_node = etree.Element(
                etree.QName(search_nsmap['xdat'], 'child_set'),
                nsmap=search_nsmap)

            container_node.append(
                build_criteria_set(sub_container_node, criteria))

        if isinstance(criteria, (tuple)):
            if len(criteria) != 3:
                raise exceptions.SyntaxError(
                    '%s should be a 3-element tuple' % str(criteria))

            constraint_node = etree.Element(
                etree.QName(search_nsmap['xdat'], 'criteria'),
                nsmap=search_nsmap)

            constraint_node.set('override_value_formatting', '0')

            schema_field_node = etree.Element(
                etree.QName(search_nsmap['xdat'], 'schema_field'),
                nsmap=search_nsmap)

            schema_field_node.text = criteria[0]

            comparison_type_node = etree.Element(
                etree.QName(search_nsmap['xdat'], 'comparison_type'),
                nsmap=search_nsmap)

            comparison_type_node.text = special_ops.get(
                criteria[1], criteria[1])

            value_node = etree.Element(
                etree.QName(search_nsmap['xdat'], 'value'),
                nsmap=search_nsmap)

            value_node.text = criteria[2].replace('*', special_ops['*'])

            constraint_node.extend([
                    schema_field_node, comparison_type_node, value_node])

            container_node.append(constraint_node)

    return container_node


def build_search_from_mongo_query(data_type, columns, filters,
                                  brief_description, long_description,
                                  allowed_users):

    def process_query(filters):
        and_ = [
            field for field in filters
            if not isinstance(filters[field], dict) and field != '$or']
        in_ = [
            field for field in filters
            if (isinstance(filters[field], dict) and
                '$in' in filters[field])]
        all_ = [
            field for field in filters
            if (isinstance(filters[field], dict) and
                '$all' in filters[field])]
        or_ = filters['$or'] if '$or' in filters else dict()

        and_x = ['AND'] + [(field, 'LIKE', filters[field]) for field in and_]

        in_x = []
        for field in in_:
            in_x.append([(field, 'LIKE', val)
                         for val in filters[field]['$in']] + ['OR'])

        all_x = []
        for field in all_:
            all_x.append([(field, 'LIKE', val)
                          for val in filters[field]['$all']] + ['AND'])

        or_x = ['OR']
        for sub_filters in or_:
            or_x.append(process_query(sub_filters))

        query = ['AND']
        if and_x != ['AND']:
            query.append(and_x)
        if in_x != []:
            query.extend(in_x)
        if all_x != []:
            query.extend(all_x)
        if or_x != ['OR']:
            query.append(or_x)

        return query

    return build_search_document(data_type, columns, process_query(filters),
                                 brief_description, long_description,
                                 allowed_users)


def build_xnat_search(data_type, columns, filters, brief_description='',
                      long_description='', allowed_users=[]):
    if isinstance(filters, dict):
        return build_search_from_mongo_query(data_type, columns, filters,
                                             brief_description,
                                             long_description,
                                             allowed_users)
    else:
        return build_search_document(data_type, columns, filters,
                                     brief_description, long_description,
                                     allowed_users)
