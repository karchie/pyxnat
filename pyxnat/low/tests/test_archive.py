import uuid

from xml.etree import ElementTree as etree

from pyxnat.client import Server
from pyxnat.utils import collections as cl
from pyxnat.low.archive import get_projects
from pyxnat.low.archive import get_project
from pyxnat.low.archive import get_project_accessibility
from pyxnat.low.archive import set_project_accessibility
from pyxnat.low.archive import get_project_archive_folder
from pyxnat.low.archive import set_project_archive_folder
from pyxnat.low.archive import get_project_prearchive_code
from pyxnat.low.archive import set_project_prearchive_code
from pyxnat.low.archive import get_project_quarantine_code
from pyxnat.low.archive import set_project_quarantine_code
from pyxnat.low.archive import get_project_users
from pyxnat.low.archive import add_project_user
from pyxnat.low.archive import remove_project_user
from pyxnat.low.archive import get_subjects
from pyxnat.low.archive import get_subject
from pyxnat.low.archive import get_subject_resources
from pyxnat.low.archive import get_subject_resource
from pyxnat.low.archive import get_subject_resource_files
from pyxnat.low.archive import get_subject_resource_file
from pyxnat.low.archive import get_experiments
from pyxnat.low.archive import get_experiment
from pyxnat.low.archive import get_experiment_resources
from pyxnat.low.archive import get_experiment_resource
from pyxnat.low.archive import get_experiment_resource_files
from pyxnat.low.archive import get_experiment_resource_file
from pyxnat.low.archive import get_assessors
from pyxnat.low.archive import get_assessor
from pyxnat.low.archive import get_assessor_resources
from pyxnat.low.archive import get_assessor_resource
from pyxnat.low.archive import get_assessor_resource_files
from pyxnat.low.archive import get_assessor_resource_file
from pyxnat.low.archive import get_scans
from pyxnat.low.archive import get_scan
from pyxnat.low.archive import get_scan_resources
from pyxnat.low.archive import get_scan_resource
from pyxnat.low.archive import get_scan_resource_files
from pyxnat.low.archive import get_scan_resource_file
from pyxnat.low.archive import create_project
from pyxnat.low.archive import delete_project
from pyxnat.low.archive import create_subject
from pyxnat.low.archive import delete_subject
from pyxnat.low.archive import create_experiment
from pyxnat.low.archive import delete_experiment
from pyxnat.low.archive import create_assessor
from pyxnat.low.archive import delete_assessor
from pyxnat.low.archive import create_reconstruction
from pyxnat.low.archive import delete_reconstruction
from pyxnat.low.archive import create_scan
from pyxnat.low.archive import delete_scan
from pyxnat.low.helpers import is_valid


server = Server('https://nosetests:nosetests@central.xnat.org')

oasis_proj = 'CENTRAL_OASIS_CS'
oasis_subj = 'OAS1_0457'
oasis_exp = 'OAS1_0457_MR1'
oasis_asm = 'OAS1_0457_MR1_APARC_LH'
oasis_asm_res = '6829'
oasis_scan = 'mpr-1'
oasis_scan_res = '938'

PA = 'PA'
PB = 'PB'

_project_id = uuid.uuid1().hex
_subject_id = uuid.uuid1().hex
_exp_id = uuid.uuid1().hex
_asm_id = uuid.uuid1().hex
_scan_id = uuid.uuid1().hex
_rec_id = uuid.uuid1().hex


def test_create_project():
    create_project(server, _project_id)
    projects = [doc['name'] for doc in get_projects(server)]
    assert PA in projects


def test_get_projects():
    projects = get_projects(server)
    assert isinstance(projects, list)
    assert isinstance(projects[0], dict)


def test_get_project():
    project = get_project(server, oasis_proj)
    etree.fromstring(project)


def test_get_project_accessibility():
    accessibility = get_project_accessibility(server, _project_id)
    assert isinstance(accessibility, (str, unicode))


def test_set_project_accessibility():
    set_project_accessibility(server, _project_id, 'private')
    accessibility = get_project_accessibility(server, _project_id)
    assert accessibility == 'private'
    set_project_accessibility(server, _project_id, 'protected')
    accessibility = get_project_accessibility(server, _project_id)
    assert accessibility == 'protected'


def test_get_project_archive_folder():
    arc = get_project_archive_folder(server, _project_id)
    assert isinstance(arc, (str, unicode))


def test_set_project_archive_foler():
    set_project_archive_folder(server, _project_id, 'arc002')
    arc = get_project_archive_folder(server, _project_id)
    assert arc == 'arc002'
    set_project_archive_folder(server, _project_id, 'arc001')
    arc = get_project_archive_folder(server, _project_id)
    assert arc == 'arc001'


def test_get_project_prearchive_code():
    code = get_project_prearchive_code(server, _project_id)
    assert isinstance(code, int)


def test_set_project_prearchive_code():
    set_project_prearchive_code(server, _project_id, 4)
    code = get_project_prearchive_code(server, _project_id)
    assert code == 4
    set_project_prearchive_code(server, _project_id, 5)
    code = get_project_prearchive_code(server, _project_id)
    assert code == 5


def test_get_project_quarantine_code():
    code = get_project_quarantine_code(server, _project_id)
    assert isinstance(code, int)


def test_set_project_quarantine_code():
    set_project_quarantine_code(server, _project_id, 1)
    code = get_project_quarantine_code(server, _project_id)
    assert code == 1
    set_project_quarantine_code(server, _project_id, 0)
    code = get_project_quarantine_code(server, _project_id)
    assert code == 0


def test_get_project_users():
    users = get_project_users(server, _project_id)
    users = [doc['login'] for doc in users]
    assert 'nosetests' in users


def test_add_project_user():
    add_project_user(server, _project_id, 'nozetests', 'members')
    users = get_project_users(server, _project_id)
    users = [(doc['login'], doc['displayname']) for doc in users]
    assert ('nozetests', 'Members') in users


def test_remove_project_user():
    remove_project_user(server, _project_id, 'nozetests', 'members')
    users = get_project_users(server, _project_id)
    users = [(doc['login'], doc['displayname']) for doc in users]
    assert not ('nozetests', 'Members') in users


def test_get_subjects():
    subjects = get_subjects(server)
    assert isinstance(subjects, list)
    assert isinstance(subjects[0], dict)


def test_get_subject():
    subject = get_subject(server, oasis_proj, oasis_subj)
    etree.fromstring(subject)


def test_get_subject_resources():
    resources = get_subject_resources(server, oasis_proj, oasis_subj)
    assert isinstance(resources, list)


def test_get_suject_resource():
    pass


def test_get_suject_resource_files():
    pass


def test_get_suject_resource_file():
    pass


def test_get_experiments():
    # get all experiments
    experiments = get_experiments(server)
    assert isinstance(experiments, list)

    # get experiments from project
    experiments = get_experiments(server, oasis_proj)
    assert isinstance(experiments, list)

    # get mrSessionData experiments
    experiments = get_experiments(server, data_type='xnat:mrSessionData')
    assert isinstance(experiments, list)
    assert experiments[0]['xsiType'] == 'xnat:mrSessionData'

    # get petSessionData experiments
    experiments = get_experiments(
        server, data_type='cnda:atlasScalingFactorData')
    print experiments
    assert isinstance(experiments, list)
    assert experiments[0]['xsiType'] == 'cnda:atlasScalingFactorData'

    # get experiments from project & subject
    experiments = get_experiments(server, oasis_proj, oasis_subj)
    assert isinstance(experiments, list)

    # get experiments from project & subject and return_field
    experiments = get_experiments(server, oasis_proj, oasis_subj,
                                  fields=['subject_ID'])
    assert isinstance(experiments, list)
    assert 'subject_ID' in experiments[0]


def test_get_experiment():
    experiment = get_experiment(server, oasis_proj, oasis_subj, oasis_exp)
    etree.fromstring(experiment)


def test_get_experiment_resources():
    resources = get_experiment_resources(server, oasis_proj, oasis_subj,
                                         oasis_exp)
    assert isinstance(resources, list)


def test_get_experiment_resource():
    pass


def test_get_experiment_resource_files():
    pass


def test_get_experiment_resource_file():
    pass


def test_get_assessors():
    assessors = get_assessors(server, oasis_proj, oasis_subj, oasis_exp)
    assert isinstance(assessors, list)


def test_get_assessor():
    assessor = get_assessor(server, oasis_proj, oasis_subj,
                            oasis_exp, oasis_asm)
    etree.fromstring(assessor)


def test_get_assessor_resources():
    resources = get_assessor_resources(server, oasis_proj, oasis_subj,
                                       oasis_exp, oasis_asm, 'in')
    assert isinstance(resources, list)
    assert resources[0]['xnat_abstractresource_id'] == \
        oasis_asm_res


def test_get_assessor_resource():
    resource = get_assessor_resource(server, oasis_proj, oasis_subj,
                                     oasis_exp, oasis_asm,
                                     'in', oasis_asm_res)
    etree.fromstring(resource)


def test_get_assessor_resource_files():
    files = get_assessor_resource_files(
        server, oasis_proj, oasis_subj,
        oasis_exp, oasis_asm,
        'in', oasis_asm_res)
    assert isinstance(files, list)


def test_get_assessor_resource_file():
    pass


def test_get_scans():
    scans = get_scans(server, oasis_proj, oasis_subj, oasis_exp)
    assert isinstance(scans, list)


def test_get_scan():
    scan = get_scan(server, oasis_proj, oasis_subj,
                    oasis_exp, oasis_scan)
    etree.fromstring(scan)


def test_get_scan_resources():
    resources = get_scan_resources(server, oasis_proj, oasis_subj,
                                   oasis_exp, oasis_scan)
    assert isinstance(resources, list)
    print resources
    assert resources[0]['xnat_abstractresource_id'] == \
        oasis_scan_res


def test_get_scan_resource():
    resource = get_scan_resource(server, oasis_proj, oasis_subj,
                                 oasis_exp, oasis_scan,
                                 oasis_scan_res)
    etree.fromstring(resource)


def test_get_scan_resource_files():
    files = get_scan_resource_files(
        server, oasis_proj, oasis_subj,
        oasis_exp, oasis_scan,
        oasis_scan_res)
    assert isinstance(files, list)


def test_get_scan_resource_file():
    pass


def test_create_subject():
    # with generated id
    create_subject(server, PA, _subject_id)
    # with custom id
    create_subject(server, PA, '%s_bis' % _subject_id,
                   subject_label='%s_bis' % _subject_id)

    subject_list = get_subjects(server)
    subjects = [doc['label'] for doc in subject_list]
    assert _subject_id in subjects
    subjects = [doc['ID'] for doc in subject_list]
    assert '%s_bis' % _subject_id in subjects


def test_create_experiment():
    # with generated id
    create_experiment(server, PA, _subject_id,
                      _exp_id, data_type='xnat:mrSessionData')
    # with custom id
    # create_experiment(server, PA, _subject_id,
    #                   experiment_id='%s_bis' % _exp_id,
    #                   experiment_label='%s_bis' % _exp_id,
    #                   data_type='xnat:mrSessionData')
    exp_list = get_experiments(server, PA)
    experiments = [doc['label'] for doc in exp_list]
    assert _exp_id in experiments
    # experiments = [doc['ID'] for doc in exp_list]
    # assert '%s_bis' % _exp_id in experiments


# def test_create_assessor():
#     # with generated id
#     create_assessor(server, PA, _subject_id,
#                     _exp_id, _asm_id, data_type='xnat:mrAssessorData')
#     # # with custom id
#     # create_assessor(server, PA, _subject_id, _exp_id,
#     #                 assessor_id='%s_bis' % _asm_id,
#     #                 assessor_label='%s_bis' % _asm_id,
#     #                 data_type='xnat:mrAssessorData')
#     asm_list = get_assessors(server, PA, _subject_id, _exp_id)
#     assessors = [doc['label'] for doc in asm_list]
#     assert _asm_id in assessors
#     # assessors = [doc['ID'] for doc in asm_list]
#     # assert '%s_bis' % _asm_id in assessors


# def test_delete_assessor():
#     asm_list = get_assessors(server, PA, _subject_id, _exp_id)
#     for asm_id in cl.distinct(asm_list, field='ID'):
#         delete_assessor(server, PA,
#                         _subject_id, _exp_id, asm_id)
#     asm_list = get_assessors(server, PA, _subject_id, _exp_id)
#     assert asm_list == []


def test_create_scan():
    create_scan(server, PA, _subject_id,
                    _exp_id, _scan_id, data_type='xnat:mrScanData')
    scan_list = get_scans(server, PA, _subject_id, _exp_id)
    scans = [doc['ID'] for doc in scan_list]
    assert _scan_id in scans


def test_delete_scan():
    scan_list = get_scans(server, PA, _subject_id, _exp_id)
    for scan_id in cl.distinct(scan_list, field='ID'):
        delete_scan(server, PA,
                        _subject_id, _exp_id, scan_id)
    scan_list = get_scans(server, PA, _subject_id, _exp_id)
    assert scan_list == []


def test_delete_experiment():
    delete_experiment(server, PA,
                      _subject_id, _exp_id)
    # delete_experiment(server, PA,
    #                   _subject_id, '%s_bis' % _exp_id)
    exp_list = get_experiments(server, PA)
    experiments = [doc['label'] for doc in exp_list]
    assert not _exp_id in experiments
    # assert not '%s_bis' % _exp_id in experiments


def test_delete_subject():
    delete_subject(server, PA, _subject_id)
    delete_subject(server, PA, '%s_bis' % _subject_id)
    subjects = [doc['label'] for doc in get_subjects(server)]
    assert not _subject_id in subjects
    assert not '%s_bis' % _subject_id in subjects


def test_delete_project():
    delete_project(server, _project_id)
    projects = [doc['name'] for doc in get_projects(server)]
    print projects
    assert not _project_id in projects
