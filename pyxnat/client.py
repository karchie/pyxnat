import os
import re
from tempfile import gettempdir

from pyxnat import http

from pyxnat.high.search import Search
from pyxnat.high.archive import Archive

from pyxnat.rest.services import version, jsession
from pyxnat.cache import DiskCache


def init_cache_dir(resource, root):
    host = re.findall('http.*//(.*)/.*$', resource.url)[0]
    host = host.replace('.', '_')
    cache_dir = '%s/%s_%s' % (root, resource.credentials[0], host)

    if not os.path.exists(cache_dir):
        os.makedirs(cache_dir)
    return cache_dir


class Server(object):

    def __init__(self, url, cache_dir=gettempdir(),
                 safe=True, fix=True, return_rsets=True, convert_xml=True):
        self.resource = http.Resource(url, session=http.Session())('data')
        self.cache = DiskCache(init_cache_dir(self.resource, cache_dir))
        self.resource.session.cache = self.cache
        self.safe = True
        self.fix = True
        self.return_rsets = return_rsets
        self.convert_xml = True
        self.search = Search(self)
        self.archive = Archive(self)
        self._connect()

    # TODO: probably something to do with the new expiration mechanism in 1.6
    def _connect(self):
        _jsession = jsession(self.resource).get()[2].read()
        self.resource.headers['cookie'] = 'JSESSIONID=%s' % _jsession

    def get_version(self):
        return version(self.resource).get()[2].read()


if __name__ == '__main__':
    from pyxnat.utils import rsets
    from high import archive
    server = Server('https://nosetests:nosetests@central.xnat.org')


    # server = Server('http://admin:admin@sandbox-16.xnat.org')

    # # Example 1 

    # for doc in server.archive.iter.scans(data_type='xnat:mrSessionData'):
    #     print doc
    #     for new in server.archive.get.scan_resources(**doc):
    #         doc.update(new)
    #         for new in server.archive.get.scan_resource_files(**doc):
    #             doc.update(new)
    #             print server.archive.get.scan_resource_file(**doc)

    # Example 2

    # for doc in server.search('xnat:mrSessionData', 
    #                          ['xnat:mrSessionData/SESSION_ID', 
    #                           'xnat:mrSessionData/PROJECT', 
    #                           'xnat:mrSessionData/SUBJECT_ID'], 
    #                          [('xnat:mrSessionData/PROJECT', 'LIKE', '%OASIS%CS%'), 'AND']
    #                          ).get():
    #     print '-' * 100
    #     print doc
    #     for a in server.archive.get.assessors(**doc):
    #         print a

    # Example 3

    # docs = server.search(
    #     'xnat:mrSessionData', 
    #     ['xnat:mrSessionData/SESSION_ID', 
    #      'xnat:mrSessionData/PROJECT', 
    #      'xnat:mrSessionData/SUBJECT_ID'], 
    #     [('xnat:mrSessionData/PROJECT', 'LIKE', '%OASIS%CS%'), 'AND']).get()

    # subjects = cl.distinct(docs, 'subject_id')[:20] # take 20 subjects ID

    # # get experiments from those subjects
    # experiments = server.archive.get.experiments(subject_id=subjects) 
    # print experiments
