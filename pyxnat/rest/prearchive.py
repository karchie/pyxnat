# ---------------------------------------------------------------------------
# Prearchive Resources
# ---------------------------------------------------------------------------

def projects(resource):
    return resource('prearchive', 'projects')

def project(resource, project_id):
    return resource('prearchive', 'projects', project_id)

def project_session(resource, project_id, timestamp, session_id):
    return resource('prearchive', 
                    'projects', project_id,
                    timestamp, session_id)

def project_session_scans(
    resource, project_id, timestamp, session_id):
    return resource('prearchive', 
                    'projects', project_id,
                    timestamp, session_id,
                    'scans')

def project_session_scan_resources(
    resource, project_id, timestamp, session_id, scan_id):
    return resource('prearchive', 
                    'projects', project_id,
                    timestamp, session_id,
                    'scans', scan_id,
                    'resources')

def project_session_scan_resource_files(
    resource, project_id, timestamp, 
    session_id, scan_id, resource_id):
    return resource('prearchive', 
                    'projects', project_id,
                    timestamp, session_id,
                    'scans', scan_id,
                    'resources', resource_id,
                    'files')

def project_session_scan_resource_file(
    resource, project_id, timestamp, 
    session_id, scan_id, resource_id, file_name):
    return resource('prearchive', 
                    'projects', project_id,
                    timestamp, session_id,
                    'scans', scan_id,
                    'resources', resource_id,
                    'files', file_name)
