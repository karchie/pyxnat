# ---------------------------------------------------------------------------
# Service Resources
# ---------------------------------------------------------------------------

def archive(resource):
    return resource('services', 'archive')

def imports(resource):
    return resource('services', 'import')

def status(resource, trx_id):
    return resource('status', trx_id)

def version(resource):
    return resource('version')

def prearchive_delete(resource):
    return resource('services', 'prearchive', 'delete')

def prearchive_move(resource):
    return resource('services', 'prearchive', 'move')

# ---------------------------------------------------------------------------
# HTTP Session Management Resources

def jsession(resource):
    return resource('JSESSION')
