# from functools import partial
from fnmatch import fnmatch

from pyxnat.low import archive
from pyxnat.utils.rsets import ResultSet, select, identifiers
from pyxnat.utils.funcs import partial
from pyxnat.fixes import archive as fixes
from pyxnat.high import helpers
from pyxnat.high.helpers import create_entity, delete_entity, exists
from pyxnat.high.helpers import get_entity


def get_projects(server,
                 accessible=True, owner=False,
                 member=True, active_since='',
                 recent=True, favorite=True):
    header = ['id', 'secondary_id']
    data = archive.get_projects(server, accessible, owner,
                                member, active_since, recent, favorite)
    data = select(data, header)
    if server.fix:
        fixes.translate(data, 'project')
    if server.return_rsets:
        return ResultSet(data)
    return data


def get_subjects(server, project_id=None, fields=None,
                 filters=None):
    header = ['ID', 'project', 'label']
    data = archive.get_subjects(server, project_id, fields, filters)
    data = select(data, header)
    if server.fix:
        fixes.translate(data, 'subject')
    if server.return_rsets:
        return ResultSet(data)
    return data


def get_experiments(server, project_id=None, subject_id=None,
                    data_type=None, fields=None, filters=None):
    header = ['ID', 'label', 'xsiType', 'project', 'subject_ID']
    data = archive.get_experiments(server, project_id, subject_id,
                                   data_type, fields, filters)
    data = select(data, header)
    if server.fix:
        fixes.translate(data, 'experiment')
    if server.return_rsets:
        return ResultSet(data)
    return data


def get_assessors(server,  project_id, subject_id,
                  experiment_id, fields=None, filters=None):
    header = ['ID', 'label', 'xsiType']
    data = archive.get_assessors(server, project_id, subject_id,
                                 experiment_id, fields, filters)
    data = select(data, header)
    if server.fix:
        fixes.translate(data, 'assessor')
    if server.return_rsets:
        return ResultSet(data)
    return data


def get_reconstructions(server,  project_id, subject_id,
                        experiment_id, fields=None, filters=None):
    header = ['ID']
    data = archive.get_reconstructions(server, project_id, subject_id,
                                       experiment_id, fields, filters)
    data = select(data, header)
    if server.fix:
        fixes.translate(data, 'reconstruction')
    if server.return_rsets:
        return ResultSet(data)
    return data


def get_scans(server, project_id, subject_id,
              experiment_id, fields=None, filters=None, fix=True):
    header = ['ID', 'xsiType']
    data = archive.get_scans(server, project_id, subject_id,
                             experiment_id, fields, filters)
    data = select(data, header)
    if fix:
        fixes.translate(data, 'scan')
    if server.return_rsets:
        return ResultSet(data)
    return data


def get_experiment_files(server, project_id, subject_id,
                         experiment_id):

    resource_header = ['xnat_abstractresource_id', 'element_name']
    file_header = ['Name', 'file_content', 'file_format',
                   'file_tags', 'cat_ID', 'Size', 'URI']

    resources = archive.get_experiment_resources(server, project_id,
                                                 subject_id, experiment_id)
    resources = select(resources, resource_header)
    fixes.translate(resources, 'resource')

    docs = []
    for resource in resources:
        data = archive.get_experiment_resource_files(server, project_id,
                                                     subject_id, experiment_id,
                                                     resource['resource_id'])
        data = select(data, file_header)
        if server.fix:
            fixes.translate(data, 'file')
        for row in data:
            doc = {
                'project_id': project_id,
                'subject_id': subject_id,
                'experiment_id': experiment_id
            }
            doc.update(resource)
            doc.update(row)
            docs.append(doc)

    if server.return_rsets:
        return ResultSet(docs)
    return docs


def get_assessor_files(server, project_id, subject_id,
                       experiment_id, assessor_id, inout):

    resource_header = ['xnat_abstractresource_id', 'element_name']
    file_header = ['Name', 'file_content', 'file_format',
                   'file_tags', 'cat_ID', 'Size', 'URI']

    resources = archive.get_assessor_resources(
        server, project_id, subject_id,
        experiment_id, assessor_id, inout)
    resources = select(resources, resource_header)
    fixes.translate(resources, 'resource')

    docs = []
    for resource in resources:
        data = archive.get_assessor_resource_files(
            server, project_id, subject_id,
            experiment_id, assessor_id,
            inout, resource['resource_id'])

        data = archive.get_experiment_resource_files(server, project_id,
                                                     subject_id, experiment_id,
                                                     resource['resource_id'])
        data = select(data, file_header)
        if server.fix:
            fixes.translate(data, 'file')
        for row in data:
            doc = {
                'project_id': project_id,
                'subject_id': subject_id,
                'experiment_id': experiment_id,
                'assessor_id': assessor_id
            }
            doc.update(resource)
            doc.update(row)
            docs.append(doc)

    if server.return_rsets:
        return ResultSet(docs)
    return docs


def get_scan_resources(server, project_id, subject_id,
                       experiment_id, scan_id, fix=True):
    header = ['xnat_abstractresource_id', 'element_name']
    data = archive.get_scan_resources(server, project_id, subject_id,
                                      experiment_id, scan_id)
    data = select(data, header)
    if fix:
        fixes.translate(data, 'resource')
    if server.return_rsets:
        return ResultSet(data)
    return data


def get_scan_resource_files(server, project_id, subject_id,
                            experiment_id, scan_id,
                            resource_id, fix=True):
    header = ['Name', 'file_content', 'file_format',
              'file_tags', 'cat_ID', 'Size', 'URI']
    data = archive.get_scan_resource_files(server, project_id, subject_id,
                                           experiment_id, scan_id, resource_id)
    data = select(data, header)
    if fix:
        fixes.translate(data, 'file')
    if server.return_rsets:
        return ResultSet(data)
    return data


def get_scan_resource_file(server, project_id, subject_id,
                           experiment_id, scan_id, resource_id,
                           file_id, path=None):

    archive.get_scan_resource_file(
        server, project_id, subject_id,
        experiment_id, scan_id, resource_id, file_id)

    return server.resource.session.cache.track(path)


def iter_assessors(server, project_id=None, subject_id=None,
                   experiment_id=None, data_type=None,
                   fields=None, filters=None, fix=True):

    experiment_header = ['project_id', 'subject_id', 'experiment_id']
    assessor_header = ['ID', 'label', 'xsiType']
    if fields is None:
        fields = []

    for experiment in get_experiments(
            server, project_id, subject_id, data_type,
            fields=['project', 'subject_ID', 'ID']):

        doc = select(experiment, experiment_header)

        if experiment_id is not None and \
           not fnmatch(doc['experiment_id'], experiment_id):
            continue

        try:
            for assessor in archive.get_assessors(server, fields=fields,
                                                  filters=filters, **doc):
                assessor = select(assessor, assessor_header)
                if fix:
                    fixes.translate(assessor, 'assessor')
                assessor.update(doc)
                yield assessor
        except Exception, e:
            if isinstance(e, KeyboardInterrupt):
                raise(e)
            else:
                print 'Warning: no assessor for %s' % doc


def iter_reconstructions(server, project_id=None, subject_id=None,
                         experiment_id=None, data_type=None, fields=None,
                         filters=None, fix=True):
    experiment_header = ['project_id', 'subject_id', 'experiment_id']

    if fields is None:
        fields = []

    for experiment in get_experiments(
        server, project_id, subject_id, data_type,
        fields=['project', 'subject_ID', 'ID']):

        doc = select(experiment, experiment_header)

        if (experiment_id is not None
            and not fnmatch(doc['experiment_id'], experiment_id)):
            continue

        try:
            for reconstruction in archive.get_reconstructions(
                server, fields=fields, filters=filters, **doc):
                reconstruction = select(reconstruction, ['ID'])
                if fix:
                    fixes.translate(reconstruction, 'reconstruction')
                reconstruction.update(doc)
                yield reconstruction
        except Exception, e:
            if isinstance(e, KeyboardInterrupt):
                raise(e)
            else:
                print 'Warning: no reconstruction for %s' % doc


def iter_scans(server, project_id=None, subject_id=None, experiment_id=None,
               data_type=None, fields=None, filters=None, fix=True):
    experiment_header = ['project_id', 'subject_id', 'experiment_id']

    if fields is None:
        fields = []

    for experiment in get_experiments(
        server, project_id, subject_id, data_type,
        fields=['project', 'subject_ID', 'ID']):

        doc = select(experiment, experiment_header)

        if (experiment_id is not None
            and not fnmatch(doc['experiment_id'], experiment_id)):
            continue

        try:
            for scan in archive.get_scans(
                server, fields=fields, filters=filters, **doc):
                scan = select(scan, ['ID'])
                if fix:
                    fixes.translate(scan, 'scan')
                scan.update(doc)
                yield scan
        except Exception, e:
            if isinstance(e, KeyboardInterrupt):
                raise(e)
            else:
                print 'Warning: no scan for %s' % doc


def iter_assessors_files(server, project_id=None,
                         subject_id=None, experiment_id=None, inout='in',
                         data_type=None, filters=None):

    for doc in iter_assessors(server, project_id, subject_id,
                              experiment_id, data_type,
                              filters=filters):

        query = identifiers(doc).update({'inout': inout})
        for sub in get_assessor_files(server, **query):
            doc.update(sub)
            yield doc


# def iter_reconstructions_files(server, project_id=None, 
#                          subject_id=None, experiment_id=None, inout='in',
#                          data_type=None, filters=None):

#     for doc in iter_reconstructions(server, project_id, subject_id, 
#                               experiment_id, data_type, filters=filters):
#         doc.update({'inout':inout})
#         for sub in get_reconstruction_resources(server, **doc):
#             doc.update(sub)
#             for sub in get_reconstruction_resource_files(server, **doc):
#                 yield doc


def iter_scans_files(server, project_id=None, subject_id=None,
                     experiment_id=None, data_type=None, filters=None):

    for doc in iter_scans(server, project_id, subject_id,
                          experiment_id, data_type, filters=filters):

        query = identifiers(doc)

        for sub in get_scan_resources(server, **query):
            doc.update(sub)
            query = identifiers(doc)
            for sub in get_scan_resource_files(server, **query):
                doc.update(sub)
                yield doc


def get_project_configuration(server, project_id):
    return {
        'accessibility': archive.get_project_accessibility(server, project_id),
        'archive': archive.get_project_archive_folder(server, project_id),
        'prearchive': archive.get_project_prearchive_code(server, project_id),
        'quarantine': archive.get_project_quarantine_code(server, project_id),
        'users': archive.get_project_users(server, project_id)
    }


def configure_project(server, project_id, set_accessibility=None,
                      set_archive=None, set_prearchive=None,
                      set_quarantine=None,
                      add_users=None, remove_users=None):

    if set_accessibility is not None:
        archive.set_project_accessibility(server, project_id,
                                          set_accessibility)
    if set_archive is not None:
        archive.set_project_archive_folder(server, project_id,
                                           set_archive)
    if set_prearchive is not None:
        archive.set_project_prearchive_code(server, project_id,
                                            set_prearchive)

    if set_quarantine is not None:
        archive.set_project_quarantine_code(server, project_id,
                                            set_quarantine)

    if add_users is not None:
        if isinstance(add_users, tuple):
            archive.add_project_user(server, project_id, *add_users)
        elif isinstance(add_users, list):
            for add_user in add_users:
                archive.add_project_user(server, project_id, *add_user)

    if remove_users is not None:
        if isinstance(remove_users, tuple):
            archive.remove_project_user(server, project_id, *remove_users)
        elif isinstance(remove_users, list):
            for remove_user in remove_users:
                archive.remove_project_user(server, project_id, *remove_user)


class Archive(object):

    def __init__(self, server):
        self.server = server

        self.exists = partial(exists, server)
        self.create_entity = partial(create_entity, server)
        self.delete_entity = partial(delete_entity, server)
        self.get_entity = partial(get_entity, server)
        self.get_projects = partial(get_projects, server)
        self.get_subjects = partial(get_subjects, server)
        self.get_experiments = partial(get_experiments, server)
        self.get_assessors = partial(get_assessors, server)
        self.get_reconstructions = partial(get_reconstructions, server)
        self.get_scans = partial(get_scans, server)
        self.iter_assessors = partial(iter_assessors, server)
        self.iter_reconstructions = partial(iter_reconstructions, server)
        self.iter_scans = partial(iter_scans, server)
        self.iter_assessors_files = partial(iter_assessors_files, server)
        self.iter_scans_files = partial(iter_scans_files, server)




if __name__ == '__main__':
    import uuid
    from collections import namedtuple
    from pyxnat import http

    url = 'https://nosetests:nosetests@central.xnat.org'
    server = namedtuple('Server', ['resource'])(http.Resource(url, None)('data'))
