import re
from xml.etree import ElementTree as etree

from pyxnat.low import helpers


def xml_to_json(xml):
    doc = {}
    root = etree.fromstring(xml)
    namespaces = root.attrib[
        '{http://www.w3.org/2001/XMLSchema-instance}schemaLocation']
    nsmap = dict(zip(
        namespaces.split()[::2],
        [n.rsplit('/', 1)[1] for n in namespaces.split()[::2]]))

    def process_node(node, key):
        ns = re.findall('^{(.*)}.*$', node.tag)[0]
        name = node.tag.replace(ns, nsmap[ns]).replace('}', ':')[1:]

        node_key = '%s/%s' % (key, name) if key else name

        node_data = None
        if node.items():
            node_data = {}
            if node.text is not None and node.text.strip():
                node_data.setdefault('value',  node.text)
            node_data.update(dict(node.items()))
        else:
            if node.text is not None and node.text.strip():
                node_data = node.text

        if not node_key in doc:
            doc[node_key] = node_data
        elif isinstance(doc[node_key], dict):
            prev = doc[node_key].copy()
            doc[node_key] = [prev, node_data]
        elif isinstance(doc[node_key], list):
            doc[node_key].append(node_data)
        elif isinstance(doc[node_key], (str, unicode)):
            prev = doc[node_key]
            doc[node_key] = [prev, node_data]

        return name

    def descend(node, key):
        for sub_node in node:
            name = process_node(sub_node, key)
            if sub_node.getchildren() != []:
                descend(sub_node, '%s/%s' % (key, name))

    name = process_node(root, '')
    descend(root, name)

    doc[name].pop('{http://www.w3.org/2001/XMLSchema-instance}schemaLocation')

    return doc


def structured_uri(resource, **identifiers):
    sort = [
        'project_id',
        'subject_id',
        'experiment_id',
        'assessor_id',
        'reconstruction_id',
        'scan_id',
        'resource_id',
        'file_id'
    ]

    if 'file_id' in identifiers:
        identifiers['file_id'] = '/'.join(identifiers['file_id'])

    components = reduce(list.__add__,
                        [[key.replace('_id', 's'), identifiers[key]]
                         for key in sort if key in identifiers])

    return resource(*components)


def get_entity(server, **identifiers):
    url = structured_uri(server.resource, **identifiers)
    data = helpers.get_entity(url)
    if server.convert_xml:
        data = xml_to_json(data)
    return data


def create_entity(server, **identifiers):
    url = structured_uri(server.resource, **identifiers)
    return helpers.create_entity(url, server.safe)


def delete_entity(server, **identifiers):
    url = helpers.structured_uri(server.resource, **identifiers)
    return helpers.delete_entity(url, server.safe)


def exists(server, **identifiers):
    url = helpers.structured_uri(server.resource, **identifiers)
    return helpers.is_valid(url)


def download_file(server, path, **identifiers):
    url = structured_uri(server.resource('archive'), **identifiers)
    helpers.download_file(url)
    return server.resource.session.cache.track(path)
