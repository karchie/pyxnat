from ..low import search
from ..utils.rsets import filter, select, ResultSet
from ..fixes import search as fixes
from .helpers import xml_to_json


def get_document_infos(server, search_id):
    doc = search.get_search_document(server, search_id)
    doc = xml_to_json(doc)
    data_type = doc['security:bundle/security:root_element_name']
    fields = zip(
        doc['security:bundle/security:search_field/security:element_name'],
        doc['security:bundle/security:search_field/security:field_ID'])
    fields = ['/'.join(field) for field in fields]

    return data_type, fields


def execute_search(server, data_type, fields=None, filters=None):
    if fields is None:
        fields = get_data_fields(server, data_type)
    if filters is None:
        filters = {'%s/ID' % data_type: '%'}
    data = search.post(server, data_type, fields, filters)
    if server.fix:
        fixes.result_fields(data_type, fields, data)
    if server.return_rsets:
        return SearchResultSet(data, server, data_type, fields, filters)
    return data


def get_data_types(server):
    data = search.get_data_types(server)
    return [row.get('ELEMENT_NAME') for row in data]


def get_data_fields(server, data_type):
    data = search.get_data_fields(server, data_type)
    return ['%s/%s' % (row.get('ELEMENT_NAME'), row.get('FIELD_ID'))
                for row in data]


def field_values(server, field):
    return search.field_values(server, field)


class Search(object):

    def __init__(self, server):
        self.server = server

    def __call__(self, data_type, fields=None, filters=None):
        return execute_search(self.server, data_type, fields, filters)

    def get_results(self, brief_description):
        saved = filter(self.saved(), 'brief_description', brief_description)
        search_id = saved[0]['id']
        data = search.get_search_results(self.server, search_id)
        if self.server.fix:
            data_type, fields = get_document_infos(self.server, search_id)
            fixes.result_fields(data_type, fields, data)
        if self.server.return_rsets:
            return ResultSet(data)
        return data

    def get_document(self, brief_description):
        saved = filter(self.saved(), 'brief_description', brief_description)
        search_id = saved[0]['id']
        data = search.get_search_document(self.server, search_id)
        if self.server.convert_xml:
            return xml_to_json(data)
        return data

    def saved(self):
        data = search.get_saved(self.server)
        data = select(data, ['id', 'brief_description', 'description'])
        if self.server.return_rsets:
            return ResultSet(data)
        return data

    def save(self, brief_description, data_type, fields,
             filters, description=None, share='private'):

        search.save(self.server, brief_description, data_type, fields,
                    filters, description, share, self.server.safe)

    def delete(self, brief_description):
        search.delete(self.server, brief_description, self.server.safe)

    def types(self):
        return get_data_types(self.server)

    def fields(self, data_type):
        return get_data_fields(self.server, data_type)

    def field_values(self, field):
        return search.field_values(self.server, field)


class SearchResultSet(ResultSet):

    def __init__(self, data, server, data_type, fields, filters):
        ResultSet.__init__(self, data)
        self.server = server
        self.data_type = data_type
        self.fields = fields
        self.filters = filters

    def save(self, brief_description, description=None,
             share='private'):
        return search.save(self.server, brief_description, self.data_type,
                           self.fields, self.filters, description,
                           share, self.server.safe)

    def __repr__(self):
        return 'SearchResultSet(%s)' % list.__repr__(self)
