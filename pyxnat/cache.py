import os
import re
import shutil
import httplib
from os import path
from time import time
from hashlib import md5


def is_file_url(url):
    url = url.split('?')[0]
    return True if re.search('.*\/.+\.\w+$', url) else False

def url_to_path(cache_dir, url):
    url = url.split('?')[0]
    root, rel_path = url.split('files')
    root_hash = md5(root).hexdigest()
    return path.join(cache_dir, '%s%s' % (root_hash, '_'.join(rel_path.split('/'))))


class DiskCache(object):
    
    def __init__(self, cache_dir, expiry=None):
        self.cache_dir = cache_dir
        self.memory = {}
        self.timestamps = {}
        self.expiry = expiry
        self.last_ = None

    def get(self, k, d=None):
        if not is_file_url(k):
 
            if self.memory.has_key(k):
                if (self.expiry is None or 
                    time() - self.timestamps[k] < self.expiry):
                    return self.memory[k]
            else:
                return
        else:
            p = url_to_path(self.cache_dir, k)
            if path.exists('%s.status' % p):
                with open('%s.header' % p, 'rb') as f:
                    header = httplib.HTTPMessage(f)
                size = int(dict(header.items())['content-length'])
                if size != path.getsize(p):
                    return
                v = []
                with open('%s.status' % p, 'rb') as f:
                    v.append(int(f.read()))
                v.append(header)
                with open('%s.data' % p, 'rb') as f:
                    p = f.read()
                with open('%s' % p, 'rb') as f:
                    v.append(f.read())
                return tuple(v)
            else:
                return

    def iteritems(self):
        return self.memory.iteritems()

    def __len__(self):
        return len(self.memory)

    def __setitem__(self, k, v):
        if not is_file_url(k):
            self.memory[k] = v
            self.timestamps[k] = time()
        else:
            try:
                p = url_to_path(self.cache_dir, k)
                with open('%s.status' % p, 'wb') as f:
                    f.write(str(v[0]))
                with open('%s.header' % p, 'wb') as f:
                    f.write(str(v[1]))
                with open('%s.data' % p, 'wb') as f:
                    f.write(p)
                with open('%s' % p, 'wb') as f:
                    if isinstance(v[2], (str, unicode)):
                        f.write(v[2])
                    else:
                        f.write(v[2].read())
            except Exception, e:
                if path.exists('%s.status' % p):
                    os.remove('%s.status' % p)
                if path.exists('%s.header' % p):
                    os.remove('%s.header' % p)
                if path.exists('%s.data' % p):
                    os.remove('%s.data' % p)
                if path.exists('%s' % p):
                    os.remove('%s' % p)
                raise Exception(e)

    def __getitem__(self, k):
        v = self.get(k)
        if v is None:
            raise IndexError(k)
        return v

    def track(self, new_path):
        if is_file_url(self.last_):
            p = url_to_path(self.cache_dir, self.last_)
            with open('%s.data' % p, 'rb') as f:
                current_p = f.read()
            if new_path is not None:
                shutil.move(current_p, new_path)
                with open('%s.data' % p, 'wb') as f:
                    f.write(new_path)
                return new_path
            else:
                return current_p
                    
    def clear(self, disk=False):
        self.memory = {}
        self.timestamps = {}

        if disk:
            shutil.rmtree(self.cache_dir)
            os.makedirs(self.cache_dir)
